<?php 
class Hello{
    function __destruct(){
        echo "Destructor<br/>";
    }
    public function myfun(){
        echo "My Function Called<br/>";
    }
    public function myfun2(){
        echo "My Function 3 Called <br/>";
    }
    function __construct($a , $b){
        echo "Constructor $a<br/>";
    }
}
$obj = new Hello(10,20);
$obj->myfun();
$obj->myfun2();
?>